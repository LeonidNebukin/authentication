package com.example.mvp.presenter

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.mvp.view.AuthView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


class AuthPresenter(private val authView : AuthView) : FirebasePresenter(), LifecycleObserver {
    private var authFirebaseListener : FirebaseAuth.AuthStateListener

    init {
        authFirebaseListener = FirebaseAuth.AuthStateListener {
            if (authFirebase.currentUser != null) {
               authView.openRepositoriesScreen()
            }
        }
    }

    fun tryLogIn(email : String, password : String) {
        when {
            email.isEmpty() -> {
                authView.showLoginError()
            }
            password.isEmpty() -> {
                authView.showPasswordError()
            }
            else -> {
                authView.showLoading()
                authFirebase.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        authView.hideLoading()
                        if (task.isSuccessful) {
                            authView.openRepositoriesScreen()
                        }
                        else
                        {
                            authView.showLoginError()
                        }
                    }
            }
        }
    }

    fun tryLogUp(email : String, password : String) {
        when {
            email.isEmpty() -> {
                authView.showLoginError()
            }
            password.isEmpty() -> {
                authView.showPasswordError()
            }
            else -> {
                authView.showLoading()
                authFirebase.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        task ->
                        authView.hideLoading()
                        if (!task.isSuccessful) {
                            authView.showLoginError()
                        }
                    }
            }
        }
    }



    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun connect() {
        authFirebase.addAuthStateListener(authFirebaseListener)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun disconnect() {
        authFirebaseListener?.let {
            authFirebase.removeAuthStateListener(it)
        }
    }
}