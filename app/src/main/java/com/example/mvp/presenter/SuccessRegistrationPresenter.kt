package com.example.mvp.presenter

import com.example.mvp.view.SuccessView

class SuccessRegistrationPresenter(private val successView: SuccessView) : FirebasePresenter()
{
    fun tryLogOut() {
        authFirebase.signOut()
        successView.openAuthenticationScreen()
    }
}