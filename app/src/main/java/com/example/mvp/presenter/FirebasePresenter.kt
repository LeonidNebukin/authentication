package com.example.mvp.presenter

import com.google.firebase.auth.FirebaseAuth

open class FirebasePresenter {
    protected val authFirebase : FirebaseAuth = FirebaseAuth.getInstance()
}