package com.example.mvp

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.mvp.presenter.AuthPresenter
import com.example.mvp.view.AuthView
import kotlinx.android.synthetic.main.activity_main.*

class AuthenticationActivity : AppCompatActivity(), AuthView {
    lateinit var presenter : AuthPresenter
    lateinit var progressDialog : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = AuthPresenter(this)
        lifecycle.addObserver(presenter)
        sign_in.setOnClickListener {
            presenter.tryLogIn(
                email = email_editText.text.toString(),
                password = password_editText.text.toString()
            )
        }

        sign_up.setOnClickListener {
            presenter.tryLogUp(
                email = email_editText.text.toString(),
                password = password_editText.text.toString()
            )
        }
    }

    override fun openRepositoriesScreen() {
        startActivity(Intent(this, SuccessfulRegistration::class.java))
    }

    override fun showLoginError() {
        Toast.makeText(baseContext, "LOGIN ERROR", Toast.LENGTH_LONG).show()
    }

    override fun showPasswordError() {
        Toast.makeText(baseContext, "PASSWORD ERROR", Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage(getString(R.string.please_wait))
        progressDialog.setIndeterminate(true)
        progressDialog.show();
    }

    override fun hideLoading() {
        if (progressDialog?.isShowing) {
            progressDialog.dismiss();
        }
    }
}