package com.example.mvp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvp.presenter.SuccessRegistrationPresenter
import com.example.mvp.view.SuccessView
import kotlinx.android.synthetic.main.activity_successfull_registration.*

class SuccessfulRegistration : AppCompatActivity(), SuccessView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfull_registration)
        val presenter = SuccessRegistrationPresenter(this)
        sign_out.setOnClickListener {
            presenter.tryLogOut()
        }
    }

    override fun openAuthenticationScreen() {
        startActivity(Intent(this, AuthenticationActivity::class.java))
    }
}