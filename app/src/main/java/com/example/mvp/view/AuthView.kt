package com.example.mvp.view

interface AuthView : LoadingView {
    fun openRepositoriesScreen()
    fun showLoginError()
    fun showPasswordError()
}