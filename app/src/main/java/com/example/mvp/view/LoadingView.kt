package com.example.mvp.view

interface LoadingView {
    fun showLoading()
    fun hideLoading()
}