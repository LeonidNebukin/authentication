package com.example.mvp.view

interface SuccessView {
    fun openAuthenticationScreen()
}